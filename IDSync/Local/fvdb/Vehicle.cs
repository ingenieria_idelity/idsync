namespace IDSync.Local.fvdb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Vehicle")]
    public partial class Vehicle
    {
        public int id { get; set; }

        [StringLength(10)]
        public string plate { get; set; }

        [StringLength(10)]
        public string capacity { get; set; }

        [StringLength(10)]
        public string color { get; set; }

        [Column(TypeName = "date")]
        public DateTime? dateSOAT { get; set; }

        [Column(TypeName = "date")]
        public DateTime? dateTM { get; set; }

        [StringLength(255)]
        public string contactName { get; set; }

        [StringLength(50)]
        public string ContactPhone { get; set; }

        public int? vType { get; set; }

        public int? vLink { get; set; }

        public int? code { get; set; }

        public int? vState { get; set; }
    }
}
