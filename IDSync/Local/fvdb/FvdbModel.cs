namespace IDSync.Local.fvdb
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class FvdbModel : DbContext
    {
        public FvdbModel()
            : base("name=FvdbConnection")
        {
        }

        public virtual DbSet<Person> Person { get; set; }
        public virtual DbSet<Vehicle> Vehicle { get; set; }
        public virtual DbSet<VehicleColor> VehicleColor { get; set; }
        public virtual DbSet<vehicleContent> vehicleContent { get; set; }
        public virtual DbSet<VehicleLink> VehicleLink { get; set; }
        public virtual DbSet<vehicleType> vehicleType { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Person>()
                .Property(e => e.CaseID)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.lastname1)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.lastname2)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.dob)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.details)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.type)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.membershipNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.address)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.phone)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.mobile)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.IBSUser)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.IDLocal)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.IDForeign)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.IDPassport)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.IDDriverLicense)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.ImageReference)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.department)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.Dependence)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.branch)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.role)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.subContractor)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.status)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.SocialSec1Code)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.SocialSec2Code)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.SocialSec3Code)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.SocialSec4Code)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.RH)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.education)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.contactName)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.contactPhone)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.Gender)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.Temperature)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.Symptom)
                .IsUnicode(false);

            modelBuilder.Entity<Vehicle>()
                .Property(e => e.plate)
                .IsUnicode(false);

            modelBuilder.Entity<Vehicle>()
                .Property(e => e.capacity)
                .IsFixedLength();

            modelBuilder.Entity<Vehicle>()
                .Property(e => e.color)
                .IsUnicode(false);

            modelBuilder.Entity<Vehicle>()
                .Property(e => e.contactName)
                .IsUnicode(false);

            modelBuilder.Entity<Vehicle>()
                .Property(e => e.ContactPhone)
                .IsUnicode(false);

            modelBuilder.Entity<VehicleColor>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<VehicleColor>()
                .Property(e => e.code)
                .IsUnicode(false);

            modelBuilder.Entity<vehicleContent>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<vehicleContent>()
                .Property(e => e.code)
                .IsUnicode(false);

            modelBuilder.Entity<VehicleLink>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<VehicleLink>()
                .Property(e => e.code)
                .IsUnicode(false);

            modelBuilder.Entity<vehicleType>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<vehicleType>()
                .Property(e => e.code)
                .IsUnicode(false);
        }
    }
}
