namespace IDSync.Local.fvdb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("VehicleColor")]
    public partial class VehicleColor
    {
        [Key]
        [StringLength(10)]
        public string name { get; set; }

        [StringLength(10)]
        public string code { get; set; }
    }
}
