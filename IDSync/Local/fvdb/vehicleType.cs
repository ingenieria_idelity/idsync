namespace IDSync.Local.fvdb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("vehicleType")]
    public partial class vehicleType
    {
        public int id { get; set; }

        [StringLength(50)]
        public string description { get; set; }

        [StringLength(50)]
        public string code { get; set; }
    }
}
