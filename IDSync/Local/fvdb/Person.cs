namespace IDSync.Local.fvdb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Person")]
    public partial class Person
    {
        public int Id { get; set; }

        [StringLength(255)]
        public string CaseID { get; set; }

        [StringLength(50)]
        public string name { get; set; }

        [StringLength(50)]
        public string lastname1 { get; set; }

        [StringLength(50)]
        public string lastname2 { get; set; }

        public bool? Synced { get; set; }

        [StringLength(255)]
        public string dob { get; set; }

        public int? rounds { get; set; }

        [StringLength(1000)]
        public string details { get; set; }

        public string type { get; set; }

        [StringLength(50)]
        public string membershipNumber { get; set; }

        [StringLength(255)]
        public string address { get; set; }

        [StringLength(50)]
        public string phone { get; set; }

        [StringLength(50)]
        public string mobile { get; set; }

        [StringLength(50)]
        public string email { get; set; }

        public DateTime? dtmUpdateDate { get; set; }

        [StringLength(50)]
        public string IBSUser { get; set; }

        public int? PersonType { get; set; }

        public int? PersonState { get; set; }

        public int? VisitorType { get; set; }

        [StringLength(200)]
        public string IDLocal { get; set; }

        [StringLength(200)]
        public string IDForeign { get; set; }

        [StringLength(200)]
        public string IDPassport { get; set; }

        [StringLength(200)]
        public string IDDriverLicense { get; set; }

        public byte? IsUserInPlace { get; set; }

        [StringLength(500)]
        public string ImageReference { get; set; }

        public DateTime? ImageReferenceLastDate { get; set; }

        [StringLength(50)]
        public string department { get; set; }

        [StringLength(100)]
        public string Dependence { get; set; }

        [StringLength(50)]
        public string branch { get; set; }

        [StringLength(50)]
        public string role { get; set; }

        public int? scheduleGroupID { get; set; }

        [StringLength(50)]
        public string subContractor { get; set; }

        [StringLength(50)]
        public string status { get; set; }

        public int? salary { get; set; }

        public int? SchedulingID { get; set; }

        [Column(TypeName = "date")]
        public DateTime? LimitDate1 { get; set; }

        [Column(TypeName = "date")]
        public DateTime? LimitDate2 { get; set; }

        [Column(TypeName = "date")]
        public DateTime? LimitDate3 { get; set; }

        public bool? Lock1 { get; set; }

        public bool? Lock2 { get; set; }

        public bool? Lock3 { get; set; }

        [StringLength(10)]
        public string SocialSec1Code { get; set; }

        [StringLength(10)]
        public string SocialSec2Code { get; set; }

        [StringLength(10)]
        public string SocialSec3Code { get; set; }

        [StringLength(10)]
        public string SocialSec4Code { get; set; }

        [StringLength(3)]
        public string RH { get; set; }

        [StringLength(50)]
        public string education { get; set; }

        [StringLength(255)]
        public string contactName { get; set; }

        [StringLength(20)]
        public string contactPhone { get; set; }

        [StringLength(10)]
        public string Gender { get; set; }

        [StringLength(4)]
        public string Temperature { get; set; }

        [StringLength(100)]
        public string Symptom { get; set; }
    }
}
