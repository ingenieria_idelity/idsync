﻿using IDSync.Local.fvdb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDSync
{
    class Program
    {
        static void Main(string[] args)
        {
            using (FvdbModel context = new FvdbModel())
            {
                try
                {
                    List<Person> person = context.Person.SqlQuery("SELECT * FROM dbo.Person").ToList();           //FirstOrDefault(r => r.Id == 2182);
                    List<Vehicle> vehicle = context.Vehicle.SqlQuery("SELECT * FROM dbo.Vehicle").ToList();
                    Console.WriteLine("name" + person.ToString());
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }

        }
    }
}
